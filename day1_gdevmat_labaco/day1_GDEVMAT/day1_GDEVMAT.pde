void setup() //gets called when the prog runs
{
  //size(1920, 1080, P3D); 
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), //moves origin to center
    0, 0, 0, 
    0, -1, 0);
  //x = width/2;
  //y = height/2;
   
}


//int x;
//int y;
void draw() //gets called every frame
{
  //circle(width/2, height/2, 50);
  //circle(x, y, 50);    y++;

  //circle(0, 0, 50);
background(130);
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadFunction();
  drawCircle();
  drawSineWave();
}

void drawCartesianPlane() {
  line(300, 0, -300, 0);
  line(0, 300, 0, -300);

  for (int i = -300; i <=300; i+=10) {
    line(i, -5, i, 5);
    line(-5, i, 5, i);
  }
}

void drawLinearFunction() {
  /* f(x) = x+2
  
  let x be 4, then y=6
  let x be -5 then y = -3
  */
  
  for(int x=-200; x< 200; x++){
  circle(x, x+2, 1);
  }
}

void drawQuadFunction(){
/*
f(x) = x^2 + 2x - 5
let x be 2 then y = 3
let x be -2 then y = 3
let x be -1 then y = 6
*/

for(float x = -300; x <300; x+=0.1){
circle(x*10, (x*x)+(2*x)-5, 1);
}
}

float r = 50;
void drawCircle(){
for(int x = 0; x<360; x++){
circle((float)Math.cos(x)*r, (float)Math.sin(x)*r, 1);
}
//r++;
}

int diameter = 16;
float time = 0.0;
float changeTime = 0.1;
float amp = 100;
float frequency = 0.5;

void drawSineWave(){
  for(int x = 0; x < 1080/diameter; x++){
  ellipse(x*diameter, amp*sin(frequency*(time+x)),diameter, diameter);
  }
  time += changeTime;
  
  
}
