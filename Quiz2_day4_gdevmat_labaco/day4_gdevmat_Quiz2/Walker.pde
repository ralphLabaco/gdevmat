class Walker{
float xPos;
float yPos;

Walker(){
xPos = 0;
yPos = 0;
}
 Walker(float x, float y){
 xPos = x;
 yPos = y;
 }
 
 //Window window = new Window();

 float r_t = 0;
 float g_t = 100;
 float b_t = 200;
 float s_d = 10;
 
 void render(){
 noStroke();
 fill(map(noise(r_t), 0, 1, 0, 255), map(noise(g_t), 0, 1, 0, 255), map(noise(b_t), 0, 1, 0, 255));
 circle(xPos, yPos, map(noise(s_d), 0, 1, 10, 100));
 
 r_t+=0.01f;
 g_t+=0.01f;
 b_t+=0.01f;
 s_d+=0.01f;
 }
 
 float xR=15;
 float yR=10;
 void walk(){
 float x_n = noise(xR);
 float y_n = noise(yR);
 xPos = map(x_n, 0, 1, Window.left, Window.right);
 yPos = map(y_n, 0, 1, Window.bottom, Window.top);
 
 xR+=0.01f;
 yR+=0.01f;
 //xPos += 0.01f;
 //yPos += 0.01f;
 }
}
