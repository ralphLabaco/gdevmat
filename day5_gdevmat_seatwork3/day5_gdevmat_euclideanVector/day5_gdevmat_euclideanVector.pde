//Day 5 euclidean vector
//vector magnitude normalization
void setup() //gets called when the prog runs
{
  //size(1920, 1080, P3D); 
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), //moves origin to center
    0, 0, 0, 
    0, -1, 0);
  //x = width/2;
  //y = height/2;
   background(0);
}

//Vector2 position = new Vector2();
//Vector2 velocity = new Vector2(5, 8);
//Walker walker = new Walker();

Vector2 mousePos(){
float x  =  mouseX - Window.windowWidth;
float y  = -(mouseY - Window.windowHeight);
return new Vector2(x, y);
}

float r_t = 10;
float g_t = 5;
float b_t = 1;
void draw(){
  background(0);
  
  Vector2 mouse = mousePos();
 
 mouse.normalize();
 mouse.mult(300);
  strokeWeight(10);
  stroke(map(noise(r_t), 0, 1, 10, 255), map(noise(g_t), 0, 1, 10, 255), map(noise(b_t), 0, 1, 10, 255));
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);
  r_t += 0.01f;
  g_t += 0.01f;
  b_t += 0.01f;
  
   strokeWeight(5);
  stroke(255);
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);
  
 mouse.normalize();
 mouse.mult(50);
   strokeWeight(12);
  stroke(135);
  strokeCap(ROUND);
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);
  
  println(mouse.mag());
  
  
  //walker.render();
  //walker.walk();
  
  //fill(150,150,150);
  //circle(position.x, position.y, 50);
  //position.add(velocity);
  //if(position.x > Window.right || position.x < Window.left){
   //velocity.x*=-1;
  //}
 // else if(position.y > Window.top || position.y < Window.bottom){
  //velocity.y*=-1;
 //}
}
