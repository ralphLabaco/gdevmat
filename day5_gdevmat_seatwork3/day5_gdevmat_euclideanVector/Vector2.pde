public class Vector2{

  public float x;
  public float y;
  
  Vector2(){
  x = 0;
  y = 0;
  }
  
  Vector2(float x, float y){
  this.x = x;
  this.y = y;
  }
  
  public void add(Vector2 u){
  
  this.x += u.x;
  this.y += u.y;
}

public void sub(Vector2 u){
this.x -= u.x;
this.y -= u.y;
}

public void mult(float scalar){
  this.x *= scalar;
  this.y *= scalar;
}

public void div(float scalar){
  this.x /= scalar;
  this.y /= scalar;
}

public float mag(){
  return sqrt((x*x)+(y*y));
}

public void normalize(){
float length = this.mag();

if(length > 0){
this.div(length);
}
}
}
