class PaintSplatter{
  float xPos, yPos;
  PaintSplatter(){
  xPos = 0;
  yPos = 0;
  }
   PaintSplatter(float x, float y){
   xPos = x;
   yPos = y;
   }
    
    void render(){
    float randSize = randomGaussian();
    noStroke();
    fill(random(255), random(255), random(255), 80);
    circle(xPos, yPos, randSize*30);
    }
    
    void splatter(){
    float randX = randomGaussian();
    float randY = randomGaussian();
    xPos = randX * 120;
    yPos = randY * 180;
    }
    
    void reset(){
    float frameLimit = frameCount % 1000;
      if (frameLimit == 0){
        background(130); 
      }
    }   
}
