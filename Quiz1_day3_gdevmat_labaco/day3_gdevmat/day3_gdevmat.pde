void setup() //gets called when the prog runs
{
  //size(1920, 1080, P3D); 
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), //moves origin to center
    0, 0, 0, 
    0, -1, 0);
  //x = width/2;
  //y = height/2;
   background(130);
}

PaintSplatter paintSplatter = new PaintSplatter();
void draw(){
//float frame = 100;
  paintSplatter.render();
  paintSplatter.splatter();
  paintSplatter.reset();
}
