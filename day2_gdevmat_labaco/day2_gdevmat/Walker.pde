class Walker{
float xPos;
float yPos;

Walker(){
xPos = 0;
yPos = 0;
}
 Walker(float x, float y){
 xPos = x;
 yPos = y;
 }
 
 void render(){
 fill(random(255), random(255), random(255), 80);
 circle(xPos, yPos, 10);
 }
 
 void walk(){
 int direction = floor(random(8));
 
 switch (direction){
 case 0:
   yPos+=10;
   break;
 case 1:
   yPos-=10;
   break;
 case 2:
   xPos+=10;
   break;
 case 3:
   xPos-=10;
   break;
 case 4:
   xPos-=10;
   yPos+=10;
   break;
 case 5:
   xPos+=10;
   yPos+=10;
   break;
 case 6:
   xPos-=10;
   yPos-=10;
   break;
 case 7:
   xPos+=10;
   yPos-=10;
   break;
 }
 
 }
}
